﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinEntry : MonoBehaviour {

    public Text display;
    public StateMachine stateMachine;
    string displayString;

    void OnEnable()
    {
        displayString = "";
        pinEntered = false;
    }

	// Update is called once per frame
	void Update () {
        display.text = displayString;
        //if(Input.anyKeyDown && pinEntered) StateMachine.ChangeScene(4);
    }

    bool pinEntered;

    public void KeypadPress(string button)
    {
        if (!pinEntered)
        {
            switch (button)
            {
                case "*":
                    if (displayString.Length < 4) displayString += "*";
                    break;
                case "clear":
                    displayString = "";
                    break;
                case "enter":
                    if (displayString.Length == 4)
                    {
                        pinEntered = true;
                        StartCoroutine(RemoveCardMessage());
                        stateMachine.NextState();
                    }
                    break;
                case "cancel":
                    displayString = "";
                    break;
            }
        }
    }

    private IEnumerator RemoveCardMessage()
    {
        char[] message = "______PIN_OK______REMOVE_CARD______TOUCH_TO_CONTINUE".ToCharArray();
        //char[] message = new char[]
        //{
        //    '_','_','_','_','_','_','P','I','N','_','O','K','_','_','_','_','_','R','E','M','O','V','E','_','C','A','R','D'
        //    ,'T','O','U','C','H','_','T','O','_','C','O','N','T','I','N','U','E'
        //};
        int i = 0;
        while (true)
        {
            char[] temp = new char[6];
            for(int j = 0; j < 6; j++)
            {
                int k = i + j;
                if (k > message.Length - 1) k -= message.Length;
                temp[j] = message[k];
            }
            string output = new string(temp);
            i++;
            if (i > message.Length - 1) i = 0;
            displayString = output;
            yield return new WaitForSeconds(0.2f);
        }

    }
}
