﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fuelPriceToggle : MonoBehaviour {

    public GameObject reward;
    public GameObject standard;

    public incrementFuel fuel;
    public UnityEngine.UI.Text estimateTextLabel;

    public void setStandard()
    {
        standard.SetActive(true);
        reward.SetActive(false);
        fuel.fuelCost = 1.779f;
        fuel.fuelTotalRequired = 15.84f;
        estimateTextLabel.text = "Estimated Fill-up Cost\n<b>$28.87 </b> ";
    }

    public void setRewards()
    {
        standard.SetActive(false);
        reward.SetActive(true);
        fuel.fuelCost = 1.739f;
        fuel.fuelTotalRequired = 15.18f;
        estimateTextLabel.text = "Estimated Fill-up Cost\n<b>$26.64 </b>";
    }
}
