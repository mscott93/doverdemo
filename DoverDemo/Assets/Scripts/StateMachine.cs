﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour {

    public List<GameObject> StateObjects;

	// Use this for initialization
	void Start () {
        currentState = state.start;
        nextState = state.start;
	}
	
    enum state
    {
        start,
        fuelSelection,
        fueling,
        payment,
        end
    }

    state currentState;
    state nextState;

	// Update is called once per frame
	void Update () {

       
        for(int i = 0; i < StateObjects.Count; i++)
        {
            if ((int)currentState == i)
                StateObjects[i].SetActive(true);
            else
                StateObjects[i].SetActive(false);
        }

        switch (currentState)
        {
            case state.start:
                break;
            case state.fuelSelection:
                GetComponent<incrementFuel>().ResetFuelCounter();
                break;
            case state.fueling:
                GetComponent<incrementFuel>().StartFueling();
                break;
            case state.payment:
                GetComponent<incrementFuel>().StopFueling();
                break;
            case state.end:
                break;
            default:
                NextState();
                nextState = state.start;
                GetComponent<incrementFuel>().StopFueling();
                break;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        currentState = nextState;
	}

    public void NextState()
    {
        nextState = currentState+1;
    }

    public void Reset()
    {
        nextState = state.start;
    }
}
