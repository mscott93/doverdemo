﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderController : MonoBehaviour {

    public List<GameObject> media;
    public GameObject left;
    public GameObject mid;
    public GameObject right;

    public int counter;
    public float timedelay = 0;

    void OnEnable()
    {
        counter = 1;

        media[0].transform.SetParent(left.transform);
        media[1].transform.SetParent(mid.transform);
        media[2].transform.SetParent(right.transform);

        media[0].transform.localPosition = Vector3.zero;
        media[1].transform.localPosition = Vector3.zero;
        media[2].transform.localPosition = Vector3.zero;

        media[0].SetActive(true);
        media[1].SetActive(true);
        media[2].SetActive(true);

        if(timedelay != 0)
        {
            StartCoroutine(slideTime());
        }
    }

    public bool animationStarted = false;

    IEnumerator slideTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(timedelay);
            SlideLeft();
        }
    } 

    public void SlideLeft()
    {
        if (!animationStarted)
        {
            animationStarted = true;
            counter = counter - 1;
            if (counter < 0)
                counter = media.Count - 1;

            GetComponent<Animator>().SetTrigger("Right");
        }
    }

    public void SlideRight()
    {
        if (!animationStarted)
        {
            animationStarted = true;
            counter = counter + 1;
            if (counter > media.Count - 1)
                counter = 0;

            GetComponent<Animator>().SetTrigger("Left");
        }
    }

    public void SlideComplete()
    {
        int idxA;
        int idxB;
        int idxC;
        foreach (GameObject g in media)
        {
            g.SetActive(false);
        }
        if(counter == 0)
        {
            idxA = media.Count - 1;
            idxB = 0;
            idxC = 1;
        }
        else if(counter == media.Count - 1)
        {
            idxA = counter - 1;
            idxB = counter;
            idxC = 0;
        }
        else
        {
            idxA = counter - 1;
            idxB = counter;
            idxC = counter + 1;
        }

        media[idxA].transform.SetParent(left.transform);
        media[idxB].transform.SetParent(mid.transform);
        media[idxC].transform.SetParent(right.transform);

        media[idxA].transform.localPosition = Vector3.zero;
        media[idxB].transform.localPosition = Vector3.zero;
        media[idxC].transform.localPosition = Vector3.zero;

        media[idxA].SetActive(true);
        media[idxB].SetActive(true);
        media[idxC].SetActive(true);

        animationStarted = false;
    }
}
