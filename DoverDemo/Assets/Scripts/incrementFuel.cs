﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class incrementFuel : MonoBehaviour {

    public UnityEngine.UI.Text fuelCostLabel;
    public UnityEngine.UI.Text fuelAmountLabel;

    bool isFueling;

    public UnityEngine.UI.Image fuelTankImage;

    public void StartFueling()
    {
        if (!isFueling)
        {
            StartCoroutine(incrementValue());
            isFueling = true;
        }
    }

    public void StopFueling()
    {
        if (isFueling)
        {
            StopAllCoroutines();
            isFueling = false;
        }
    }

    public void ResetFuelCounter()
    {
        value = 0;
        string s = (value * fuelCost).ToString("00.00");
        fuelFull = false;
        if (s.Length > 1)
        {
            fuelCostLabel.text = "<size=50>$</size>" + s.Split('.')[0] + "<size=50>" + s.Split('.')[1] + "</size>";
            fuelAmountLabel.text = value.ToString("0.000");
        }      
    }

    float value = 0.0f;
    public float fuelCost = 1.779f;

    public float fuelTotalRequired = 15.84f;

    bool fuelFull = false;
    IEnumerator incrementValue()
    {
        while (!fuelFull)
        {
            yield return new WaitForSeconds(0.01f);
            string s = (value*fuelCost).ToString("00.00");        
            if (s.Length > 1)
            {
                fuelCostLabel.text = "<size=50>$</size>" + s.Split('.')[0] + "<size=50>" + s.Split('.')[1] + "</size>";
                fuelAmountLabel.text = value.ToString("0.000");
            }
            if (value > fuelTotalRequired)
                fuelFull = true;
            value += 0.006f;
            fuelTankImage.fillAmount = 0.4f + ((value / fuelTotalRequired) * 0.6f);
        }
    }
}
