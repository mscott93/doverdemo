﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class advertVideoPlayer : MonoBehaviour {

    public List<VideoClip> clips;
    int clipNumber = 0;

    bool nextClipStarted = false;
	// Update is called once per frame
	void Update () {
        if (nextClipStarted)
        {
            if (!GetComponent<VideoPlayer>().isPlaying)
            {
                if (clipNumber == clips.Count - 1)
                    clipNumber = 0;
                else
                    clipNumber++;
                GetComponent<VideoPlayer>().clip = clips[clipNumber];
                GetComponent<VideoPlayer>().Play();
                nextClipStarted = false;
            }
        }
        else
        {
            if (GetComponent<VideoPlayer>().isPlaying)
                nextClipStarted = true;
        }
	}
}